# Path to this plugin
# PROTOC_GEN_TS_PATH="./node_modules/.bin/protoc-gen-ts"

# Directory to write generated code to (.js and .d.ts files)
# OUT_DIR="./Generated"

# protoc \
#     --plugin="protoc-gen-ts=${PROTOC_GEN_TS_PATH}" \
#     --js_out="import_style=commonjs,binary:${OUT_DIR}" \
#     --ts_out="${OUT_DIR}" \
#     ./Proto/ChatProto.proto /*.proto    "proto:gen": "proto-loader-gen-types --grpcLib=@grpc/grpc-js --outDir=Proto/ Proto/ChatProto.proto",



# "proto:gen": "proto-loader-gen-types --grpcLib=@grpc/grpc-js --outDir=Proto/ Proto/ChatProto.proto",
protoc -I=. ./Proto/*.proto \
  --js_out=import_style=commonjs,binary:./Generated \
  --grpc-web_out=import_style=commonjs,mode=grpcweb:./Generated/ 


