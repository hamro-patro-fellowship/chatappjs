const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
const path = require('path');

const PROTO_PATH = './Proto/ChatProto.proto';
const PORT = 8080;
const SERVER_URI = `0.0.0.0:${PORT}`;

const usersInChat = [];
const observers = [];

const packageDefinition = protoLoader.loadSync(
  path.resolve(__dirname, PROTO_PATH)
);
const protoDescriptor = grpc.loadPackageDefinition(packageDefinition);

const join = (req, res) => {
  const user = req.request;
  console.log(user);
  // Check Username already exists;
  const userExist = usersInChat.find((_user) => _user.name == user.name);
  if (!userExist) {
    usersInChat.push(user);
    res(null, {
      msg: 'Success, User Joined the chat',
    });
  } else {
    res(null, { msg: 'user already exists' });
  }
};

const sendMsg = () => {
  console.log('Joined Called send msg');
};

const receiveMsg = () => {
  console.log('Joined Called receive msg');
};

const getAllUsers = () => {
  console.log('Joined Called getall users');
};

// Main Function, Runs server
const main = () => {
  const server = new grpc.Server();

  server.addService(protoDescriptor.ChatService.service, {
    join,
    sendMsg,
    getAllUsers,
    receiveMsg,
  });

  server.bindAsync(
    SERVER_URI,
    grpc.ServerCredentials.createInsecure(),
    (err, port) => {
      if (err) {
        console.error(err);
        return;
      }

      console.log(`Server running in port: ${port}`);
      server.start();
    }
  );
};

main();
