const path = require('path');
const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');

const PROTO_PATH = './Proto/ChatProto.proto';
const PORT = 8080;
const SERVER_URI = `0.0.0.0:${PORT}`;

const packageDefinition = protoLoader.loadSync(
  path.resolve(__dirname, PROTO_PATH)
);
const protoDescriptor = grpc.loadPackageDefinition(packageDefinition);

const client = new protoDescriptor.ChatService(
  SERVER_URI,
  grpc.credentials.createInsecure()
);

client.join(
  {
    id: '420',
    name: 'sabin',
  },
  (err, res) => {
    console.log(err, res);
    if (err) {
      console.error(err);
      return;
    } else {
      console.log(res);
    }
    // var cs = client.receiveMsg({
    //   user: 'op',
    // });
    // cs.on('data', (data) => {
    //   console.log(data);
    // });
  }
);
