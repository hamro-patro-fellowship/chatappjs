// Original file: Proto/ChatProto.proto


export interface JoinResponse {
  'error'?: (number);
  'msg'?: (string);
}

export interface JoinResponse__Output {
  'error'?: (number);
  'msg'?: (string);
}
