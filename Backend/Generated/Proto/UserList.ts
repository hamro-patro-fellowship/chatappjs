// Original file: Proto/ChatProto.proto

import type { User as _User, User__Output as _User__Output } from './User';

export interface UserList {
  'users'?: (_User)[];
}

export interface UserList__Output {
  'users'?: (_User__Output)[];
}
