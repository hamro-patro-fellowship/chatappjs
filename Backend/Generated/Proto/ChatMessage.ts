// Original file: Proto/ChatProto.proto


export interface ChatMessage {
  'from'?: (string);
  'msg'?: (string);
  'time'?: (string);
}

export interface ChatMessage__Output {
  'from'?: (string);
  'msg'?: (string);
  'time'?: (string);
}
