module.exports = {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		extend: {
			backgroundImage: {
				'chat-image': "url('./src/images/chat.jpg')"
			}
		}
	},
	plugins: []
};
